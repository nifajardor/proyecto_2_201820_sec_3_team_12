package model.data_structures;

import java.util.Iterator;
import java.util.ListIterator;
/*
 * Nota: La implementacion de esta clase fue sacada del libro Algorithms de Robert SedgeWick y Kevin Wayne
 */
public class Queue<T> implements Iterable<T> {
	private Nodo<T> primero;
	private Nodo<T> ultimo;
	private int tamanio;
	public static class Nodo<T>{
		private T elemento;
		private Nodo<T> siguiente;
	}
	public Queue() {
		primero = null;
		ultimo = null;
		tamanio = 0;
	}
	public boolean isEmpty() {
		boolean r = false;
		if(primero == null) {
			r = true;
		}
		return r;
	}
	public int size() {
		return tamanio;
	}
	public T peek() {
		T r = null;
		if(primero != null) {
			r = primero.elemento;
		}
		return r;
	}
	public void enqueue(T pElem) {
		Nodo<T> temp = ultimo;
		ultimo = new Nodo<T>();
		ultimo.elemento = pElem;
		ultimo.siguiente = null;
		if(isEmpty()) {
			primero = ultimo;
		}
		else {
			temp.siguiente = ultimo;
		}
		tamanio++;
	}
	public T dequeue() {
		T r = null;
		if(!isEmpty()) {
			r = primero.elemento;
			primero = primero.siguiente;
			tamanio--;
			if(isEmpty()) {
				ultimo = null;
			}
		}
		return r;
	}
	
	public Iterator<T> iterator(){
		return new ListIterator<T>(primero);
	}
	
	public class ListIterator<T> implements Iterator<T>{
		private Nodo<T> actual;
		public ListIterator(Nodo<T> pPrimero){
			actual = pPrimero;
		}
		public boolean hasNext() {
			boolean r = false;
			if(actual != null) {
				r = true;
			}
			return r;
		}
		public T next() {
			T r = null;
			if(hasNext()) {
				r = actual.elemento;
				actual = actual.siguiente;
			}
			return r;
		}
		
	}
	
}
