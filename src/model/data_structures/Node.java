package model.data_structures;

public class Node<E extends Comparable<E>>  {

	private Node<E> siguiente;
	private Node<E> anterior;
	private Comparable elemento;
	
	public Node(Comparable pElem) {
		elemento = pElem;
		anterior = null;
		siguiente = null;
	}
	
	public Comparable darElemento() {
		return elemento;
	}
	public Node<E> darSig() {
		return siguiente;
	}
	public Node<E> darAnt() {
		return anterior;
	}
	public void cambiarSig(Node<E> pSig) {
		siguiente = pSig;
	}
	public void cambiarAnt(Node<E> pAnt) {
		anterior = pAnt;
	}
	public void cambiarElem(Comparable elem) {
		elemento=elem;
	}
	public void imprimirr() {
		System.out.println(elemento.toString());
	}

	
	
	public Node eliminar() {
		
		if(siguiente !=null && anterior == null) {
			siguiente.cambiarAnt(null);
		}
		else if(siguiente == null && anterior != null) {
			anterior.cambiarSig(null);
		}
		else {
			siguiente.cambiarAnt(anterior);
			anterior.cambiarSig(siguiente);
		}
		return siguiente;
	}
	
	
	

	
	}
	

