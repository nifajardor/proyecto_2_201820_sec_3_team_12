package model.logic;

import API.IManager;
import controller.Controller.ResultadoCampanna;
import model.data_structures.ArregloFlexible;
import model.data_structures.ICola;
import model.data_structures.ILista;
import model.data_structures.Node;
import model.data_structures.Queue;
import model.data_structures.RedBlackBST;
import model.data_structures.SeparateChainingHash;
import model.vo.Bike;
import model.vo.BikeRoute;

import model.vo.Sector;
import model.vo.Station;
import model.vo.Trip;

import java.io.FileReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.opencsv.CSVReader;

public class Manager implements IManager {
	public static final String RUTA_JSON = "data/CDOT_Bike_Routes_2014_1216.json";
	//Ruta del archivo de datos 2017-Q1
	// TODO Actualizar
	public static final String TRIPS_Q1 = "data/Divvy_Trips_2017_Q1.csv";

	//Ruta del archivo de trips 2017-Q2
	// TODO Actualizar	
	public static final String TRIPS_Q2 = "data/Divvy_Trips_2017_Q2.csv";

	//Ruta del archivo de trips 2017-Q3
	// TODO Actualizar	
	public static final String TRIPS_Q3 = "data/Divvy_Trips_2017_Q3.csv";

	//Ruta del archivo de trips 2017-Q4
	// TODO Actualizar	
	public static final String TRIPS_Q4 = "data/Divvy_Trips_2017_Q4.csv";

	//Ruta del archivo de trips 2017-Q1-Q4
	// TODO Actualizar	
	public static final String TRIPS_Q1_Q4 = "Ruta Trips 2017-Q1-Q4 en directorio data";

	//Ruta del archivo de stations 2017-Q1-Q2
	// TODO Actualizar	
	public static final String STATIONS_Q1_Q2 = "data/Divvy_Stations_2017_Q1Q2.csv";

	//Ruta del archivo de stations 2017-Q3-Q4
	// TODO Actualizar	
	public static final String STATIONS_Q3_Q4 = "data/Divvy_Stations_2017_Q3Q4.csv";
	//Formato de la fecha
	public static final String DATE_PATTERN = "MM/dd/yyyy HH:mm:ss";

	public static final int RADIO_TIERRA = 6371;



	private Queue tripQueue;
	private Queue statQueue;
	private ArregloFlexible<Trip> arregloTrips;
	private ArregloFlexible<Station> arregloStats;
	private ArregloFlexible<Station> arregloBikes;
	private ArrayList<BikeRoute> muestra=new ArrayList<BikeRoute>();
	private int index;
	private int tamStack;
	private ArrayList[] ciclorutas;
	private double minLat;
	private double maxLat;
	private double minLong;
	private double maxLong;
	private Sector[][] sectores;
	private ArrayList<String> idCiclovias;
	private ArrayList<String> coordenadas;
	private RedBlackBST<String,Bike> bicisPorTiempo;
	private SeparateChainingHash<String, RedBlackBST<String, Trip>> paraElB2;
	

	public void cargarDatos(String rutaTrips, String rutaStations, String rutaJson) {
		cargarViajes(rutaTrips, rutaStations);
		cargarJson(rutaJson);
	}
	public void cargarJson(String rutaJson) {
		idCiclovias = new ArrayList<String>();
		coordenadas = new ArrayList<String>();
		maxLat = 0;
		maxLong= -100;
		minLat = 100;
		minLong = 0;
		Gson gson=new Gson();
		JsonReader reader;
		try {
			reader = new JsonReader(new FileReader(rutaJson));

			ciclorutas=gson.fromJson(reader, ArrayList[].class);
			System.out.println(ciclorutas.length);
			for(int i=0;i<ciclorutas.length;i++){
				muestra.add(new BikeRoute((String)ciclorutas[i].get(1), (String)ciclorutas[i].get(8),(String)ciclorutas[i].get(9),(String) ciclorutas[i].get(11),(String) ciclorutas[i].get(12), (String)ciclorutas[i].get(15)));
				String geom = muestra.get(i).darGeom();
				String infoo = geom.substring(12);
				String info = infoo.substring(0,infoo.length()-1);
				String[] pares = info.split(", ");
				idCiclovias.add(muestra.get(i).darId());
				idCiclovias.add(muestra.get(i).darId());
				String coordIniciales = pares[0];
				String coordFinales = pares[pares.length-1];
				coordenadas.add(coordIniciales);
				coordenadas.add(coordFinales);
				for(int j = 0;j<pares.length;j++) {
					String[] parAct = pares[j].split(" ");
					double longAct = Double.parseDouble(parAct[0]);
					double latAct = Double.parseDouble(parAct[1]);
					if(longAct<minLong) {
						minLong = longAct;
					}
					if(longAct>maxLong) {
						maxLong = longAct;
					}
					if(latAct<minLat) {
						minLat = latAct;
					}
					if(latAct>maxLat) {
						maxLat = latAct;
					}

				}

			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void cargarViajes(String rutaTrips, String rutaStations) {
		try {
			
			
			String[] trip;
			String[] stat;
		
			
			arregloStats = new ArregloFlexible<Station>(600);
			arregloTrips = new ArregloFlexible<Trip>(1800000);
			tripQueue = new Queue();
			statQueue = new Queue();
			DateTimeFormatter formato = DateTimeFormatter.ofPattern(DATE_PATTERN);
			int cont = 0;
			for(int i=0;i<rutaStations.split(":").length;i++){
				CSVReader lectorStations = new CSVReader(new FileReader(rutaStations.split(":")[i]));
				stat = lectorStations.readNext();
			while((stat = lectorStations.readNext()) != null) {
				int id = Integer.parseInt(stat[0]);
				String statName = stat[1];
				String[]fecha=stat[6].split("(?=\\s)");
				double lat = Double.parseDouble(stat[3]);
				double lon = Double.parseDouble(stat[4]);
				LocalDateTime startDate = convertirFecha_Hora_LDT(fecha[0], fecha[1]);
				Station estacion = new Station(id, statName, startDate, lat, lon);
				String info = estacion.toString();
				Node nodo = new Node<>(estacion);

				//statQueue.enqueue(nodo);
				arregloStats.agregarElem(estacion);

				cont++;
			}
			}
			int cont2 = 0;
			for(int i=0;i<rutaTrips.split(":").length;i++){
				CSVReader lectorTrips = new CSVReader(new FileReader(rutaTrips.split(":")[i]));
				trip = lectorTrips.readNext();
			while((trip = lectorTrips.readNext()) != null) {
				int tripID = Integer.parseInt(trip[0]);
				String[] fecha=trip[1].split("(?=\\s)");
				String[] fecha1=trip[2].split("(?=\\s)");
				LocalDateTime startTime = convertirFecha_Hora_LDT(fecha[0], fecha[1]);
				LocalDateTime endTime = convertirFecha_Hora_LDT(fecha1[0], fecha1[1]);
				int bikeID = Integer.parseInt(trip[3]);
				int tripDuration = Integer.parseInt(trip[4]);
				int startStatID = Integer.parseInt(trip[5]);
				int endStatID = Integer.parseInt(trip[7]);
				String gender;
				if(trip[10].equals(Trip.MALE))
					gender = Trip.MALE;
				else if(trip[10].equals(Trip.FEMALE))
					gender = Trip.FEMALE;
				else
					gender = Trip.UNKNOWN;

				Trip viaje = new Trip(tripID, startTime, endTime, bikeID, tripDuration, startStatID, endStatID, gender);
				Node nodo = new Node<>(viaje);
				//tripQueue.enqueue(nodo);
				arregloTrips.agregarElem(viaje);

				cont2++;
			}
			}
			System.out.println("Total trips cargados en el sistema: " + cont2);
			System.out.println("Total estaciones cargadas en el sistema: "+ cont);

		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		cargarBicisEnArbolB1();
	}
	private static LocalDateTime convertirFecha_Hora_LDT(String fecha, String hora)
	{
		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[0]);
		int dia = Integer.parseInt(datosFecha[1]);
		int horas = Integer.parseInt(datosHora[0].trim());
		int minutos = Integer.parseInt(datosHora[1]);
		int segundos = 0;

		if(datosHora.length>2){
			segundos=Integer.parseInt(datosHora[2]);
		}

		return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
	}


	public double distanciaViaje(Trip trip){
		double longitudStart=0;
		double longitudEnd=0;
		double latitudStart=0;
		double latitudEnd=0;
		double distancia=0;
		for(int i=0;i<arregloStats.darTamAct();i++){
			if(((Station)arregloStats.darElementoI(i)).getStationId()==trip.getStartStationId()){
				latitudStart=((Station)arregloStats.darElementoI(i)).getLat();
				longitudStart=((Station)arregloStats.darElementoI(i)).getLong();
			}
			if(((Station)arregloStats.darElementoI(i)).getStationId()==trip.getEndStationId()){
				latitudEnd=((Station)arregloStats.darElementoI(i)).getLat();
				longitudEnd=((Station)arregloStats.darElementoI(i)).getLong();
			}
		}
		distancia=distancia(latitudStart, longitudStart, latitudEnd, longitudEnd);
		return distancia;
	}


	public double distancia(double lat1, double long1, double lat2, double long2) {
		double deltaLat = Math.toRadians((lat2-lat1));
		double deltaLong = Math.toRadians((long2-long1));

		lat1 = Math.toRadians(lat1);
		lat2 = Math.toRadians(lat2);
		double a = harvesin(deltaLat) + Math.cos(lat1)*Math.cos(lat2)*harvesin(deltaLong);
		double c = 2*Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		return 1000*RADIO_TIERRA*c;
	}
	public double harvesin(double valor) {
		return Math.pow(Math.sin(valor / 2), 2);
	}

	

	@Override
	public ICola<Trip> A1(int n, LocalDate fechaTerminacion) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<Trip> A2(int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<Trip> A3(int n, LocalDate fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void cargarBicisEnArbolB1() {
		bicisPorTiempo = new RedBlackBST<String,Bike>();
		ArregloFlexible<Bike> bicis = new ArregloFlexible<Bike>(804002);
		for(int i =0;i<arregloTrips.darTamAct();i++) {
			Trip viajeAct = (Trip) arregloTrips.darElementoI(i);
			int idAct = viajeAct.getBikeId();
			int dur = viajeAct.getTripDuration();
			boolean esta = false;
			for(int j=0;j<bicis.darTamAct()&&!esta;j++) {
				Bike bici = (Bike) bicis.darElementoI(j);
				if(bici.getBikeId()==idAct) {
					bici.agregarDuracion(dur);
					esta = true;
				}
			}
			if(!esta) {
				bicis.agregarElem(new Bike(idAct, 0, 0, dur));
			}
		}
		int a =0;
		for(int i=0;i<bicis.darTamAct();i++) {
			Bike act = (Bike) bicis.darElementoI(i);
			int dur = act.getTotalDuration();
			a=dur;
			bicisPorTiempo.put(dur+"", act);
			System.out.println(i+"");
		}
		System.out.println(bicisPorTiempo.get(a+"").toString());
		
	}
	@Override
	public ILista<Bike> B1(int limiteInferior, int limiteSuperior) {
		
		// TODO Auto-generated method stub
		String tiempo1= limiteInferior+"";
		String tiempo2 = limiteSuperior+"";
		Queue<String> llaves=(Queue<String>) bicisPorTiempo.keys(tiempo1, tiempo2);
		if(bicisPorTiempo.keys(tiempo1, tiempo2)!=null){
			System.out.println("EXISTEEEEEEEEE");
			for(String a:llaves) {
				Bike bici= bicisPorTiempo.get(a);
				System.out.println(bici.toString());
			}
//			for(Object key:bicisPorTiempo.keys(tiempo1, tiempo2)){
//				System.out.println(key.toString());
//			}
		}else{
			System.out.println("No existe ninguna llave id en ese rango");
		}

		return null;
	}

	public void cargarParaB2() {
		
	}
	@Override
	public ILista<Trip> B2(String estacionDeInicio, String estacionDeLlegada, int limiteInferiorTiempo, int limiteSuperiorTiempo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int[] B3(String estacionDeInicio, String estacionDeLlegada) {
		// TODO Auto-generated method stub
		return new int[] {0, 0};
	}

	@Override
	public ResultadoCampanna C1(double valorPorPunto, int numEstacionesConPublicidad, int mesCampanna) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double[] C2(int LA, int LO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int darSector(double latitud, double longitud) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ILista<Station> C3(double latitud, double longitud) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<BikeRoute> C4(double latitud, double longitud) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<BikeRoute> C5(double latitudI, double longitudI, double latitudF, double longitudF){
		// TODO Auto-generated method stub
		return null;
	}

	
}
