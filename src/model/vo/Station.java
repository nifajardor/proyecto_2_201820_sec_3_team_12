package model.vo;

import java.time.LocalDateTime;

public class Station implements Comparable<Station> {
	private int stationId;
	private String stationName;
	private LocalDateTime startDate;
	private double lat;
	private double longit;
	//TODO Completar

	public Station(int stationId, String stationName, LocalDateTime startDate, double latit, double longitud) {
		this.stationId = stationId;
		this.stationName = stationName;
		this.startDate = startDate;
		lat = latit;
		longit = longitud;
	}

	@Override
	public int compareTo(Station o) {
		// TODO Auto-generated method stu
		if(startDate.isAfter(o.getStartDate())){
			return 1;
		}else if(startDate.isBefore(o.getStartDate())){
			return -1;
		}else{
			return 0;
		}
		
	}
	public double getLat() {
		return lat;
	}
	public double getLong() {
		return longit;
	}
	public LocalDateTime getStartDate() {
		return startDate;
	}

	public int getStationId() {
		return stationId;
	}

	public String getStationName() {
		return stationName;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return stationId + ", " + stationName;
	}
}
