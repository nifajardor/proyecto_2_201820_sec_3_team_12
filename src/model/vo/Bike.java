package model.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

public class Bike implements Comparable<Bike>{

	public static final int ORDENAR_VIAJES = 0;
	public static final int ORDENAR_DISTANCIA = 1;
	public static final int ORDENAR_DURACION = 2;
    private int bikeId;
    private int totalTrips;
    private double totalDistance;
    private int totalDuration;

    public Bike(int bikeId, int totalTrips, double totalDistance, int ptotalDuration) {
        this.bikeId = bikeId;
        this.totalTrips = totalTrips;
        this.totalDistance = totalDistance;
        this.totalDuration = ptotalDuration;
    }

    public int compareTo(Bike o, int tipo) {
    	// TODO completar
    	int retorno = 0;
    	if(tipo == ORDENAR_DISTANCIA) {
    		retorno = compararDistanciaTotal(o);
    	}
    	else if(tipo == ORDENAR_DURACION) {
    		retorno = compararDuracionTotal(o);
    	}
    	else {
    		retorno = compararNumeroViajes(o);
    	}
        return retorno;
    }
    public int compararDistanciaTotal(Bike o ) {
    	int retorno = 0;
    	if(totalDistance>o.getTotalDistance()) {
    		retorno = 1;
    	}
    	else if(totalDistance<o.getTotalDistance()) {
    		retorno = -1;
    	}
        return retorno;
    }
    public int compararDuracionTotal(Bike o) {
    	int retorno = 0;
    	if(totalDuration>o.getTotalDuration()) {
    		retorno = 1;
    	}
    	else if(totalDuration<o.getTotalDuration()) {
    		retorno = -1;
    	}
        return retorno;
    }
    public int compararNumeroViajes(Bike o) {
    	int retorno = 0;
    	if(totalTrips>o.getTotalTrips()) {
    		retorno = 1;
    	}
    	else if(totalTrips<o.getTotalTrips()) {
    		retorno = -1;
    	}
        return retorno;

    }
    public void agregarViaje() {
    	totalTrips++;
    }
    public void agregarDistancia(double dist) {
    	totalDistance += dist;
    }
    public void agregarDuracion(int time) {
    	totalDuration += time;
    }

    public int getBikeId() {
        return bikeId;
    }

    public int getTotalTrips() {
        return totalTrips;
    }

    public double getTotalDistance() {
        return totalDistance;
    }
    public int getTotalDuration() {
    	return totalDuration;
    }
    @Override
    public String toString() {
    	// TODO Auto-generated method stub
    	return "id: " + bikeId + ", duracion: " + totalDuration;
    }

    
    public void aumentarDuracion(int a){
    	totalDuration+=a;
    }

    public void aumentarDistancia(double a){
    	totalDistance+=a;
    }

	@Override
	public int compareTo(Bike arg0) {
		int aRet = 0;
		if(totalTrips > arg0.getTotalTrips()) {
			aRet = 1;
		}
		else if(totalTrips < arg0.getTotalTrips()) {
			aRet = -1;
		}
		return aRet;
	}
}
